package main

import (
	"encoding/json"
	"fmt"
	"log"
	"path/filepath"
	"strings"

	"github.com/xuri/excelize/v2"
)

type Formatted struct {
	CreatedAt int
	Worksheet string
	Workbook  string
	Target    string
	Delimiter []string
	Rows      [][]string
}

type Workbook struct {
	CreatedAt int
	Worksheet string
	Workbook  string
	Delimiter []string
}

func createPath(target string, workbook string) string {
	return filepath.Join(target, filepath.Base(workbook))
}

func (a *App) Save(target string, workbook string, worksheet string, rows *[][]string) {
	f := excelize.NewFile()
	path := createPath(target, workbook)

	for rowNumber, row := range *rows {
		for colNumber, col := range row {
			// prevent using 0, as the sheet starts at 1
			coor, err := excelize.CoordinatesToCellName(colNumber+1, rowNumber+1)

			if err != nil {
				continue
			}

			if err := f.SetCellValue(worksheet, coor, col); err != nil {
				fmt.Print(err.Error())
			}
		}
	}

	if err := f.SaveAs(path); err != nil {
		log.Fatal(err)
	}
}

func (a *App) FormatRow(row []string, delimiter string) []string {
	newRow := []string{}

	for _, column := range row {
		newColumn := strings.Split(column, delimiter)
		newRow = append(newRow, newColumn...)
	}

	return newRow
}

func (a *App) FormatSheet(value string) (*Formatted, error) {
	var data Formatted
	json.Unmarshal([]byte(value), &data)

	newRows := [][]string{}
	formatted := Formatted{
		CreatedAt: data.CreatedAt,
		Worksheet: data.Worksheet,
		Workbook:  data.Workbook,
		Delimiter: data.Delimiter,
		Target:    data.Target,
	}

	rows, err := a.ReadFile(data.Workbook, data.Worksheet)

	if err != nil {
		return nil, err
	}

	for _, row := range rows {
		for _, delimiter := range data.Delimiter {
			newRows = append(newRows, a.FormatRow(row, delimiter))
		}
	}

	formatted.Rows = newRows

	return &formatted, nil
}

func (a *App) FormatExcel(value string) error {
	formatted, err := a.FormatSheet(value)

	if err != nil {
		return err
	}

	a.Save(formatted.Target, formatted.Workbook, formatted.Worksheet, &formatted.Rows)

	return nil
}
