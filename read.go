package main

import (
	"fmt"

	"github.com/xuri/excelize/v2"
)

func (a *App) ReadFile(name string, worksheet string) ([][]string, error) {
	file, err := excelize.OpenFile(trimPrefix(name))

	if err != nil {
		return nil, err
	}

	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	rows, err := file.GetRows(worksheet, excelize.Options{RawCellValue: true})

	if err != nil {
		return nil, err
	}

	return rows, nil
}
