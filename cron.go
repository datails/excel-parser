package main

import (
	"os"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/gobwas/glob"
	"github.com/google/uuid"
)

var s = gocron.NewScheduler(time.Local)

func (a *App) Schedule(cron string, workbook string) error {
	_, err := s.Cron(cron).SingletonMode().Tag(uuid.NewString()).Do(func() { a.FormatExcel(workbook) })

	if err != nil {
		return err
	}

	return nil
}

func (a *App) ClearAllSchedulers() {
	s.Clear()
}

func (a *App) ListSchedulers() []*gocron.Job {
	return s.Jobs()
}

func (a *App) AttachScheduleToFiles(cron string, dir string) error {
	g := glob.MustCompile("{*.xlsx, *.xls}")
	dirs, err := os.ReadDir(dir)

	if err != nil {
		return err
	}

	for _, e := range dirs {
		if g.Match(e.Name()) {
			if err := a.Schedule(cron, e.Name()); err != nil {
				return err
			}
		}
	}

	return nil
}
