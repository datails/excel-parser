package main

import (
	"fmt"
	"strings"

	"github.com/wailsapp/wails/v2/pkg/runtime"
	"github.com/xuri/excelize/v2"
)

func (a *App) SelectWorkBook() (string, error) {
	selection, err := runtime.OpenFileDialog(a.ctx, runtime.OpenDialogOptions{
		Title: "Select file",
		Filters: []runtime.FileFilter{
			{
				DisplayName: "Excel (*.xls;*.xlsx)",
				Pattern:     "*.xls;*.xlsx",
			},
		},
	})

	if err != nil {
		return "", err
	}

	return selection, nil
}

func trimPrefix(filePath string) string {
	return strings.TrimPrefix(filePath, "file:/")
}

func (a *App) Worksheets(name string) ([]string, error) {
	file, err := excelize.OpenFile(trimPrefix(name))

	if err != nil {
		return nil, err
	}

	defer func() {
		// Close the spreadsheet.
		if err := file.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	return file.GetSheetList(), nil
}

func (a *App) SelectDir() (string, error) {
	dir, err := runtime.OpenDirectoryDialog(a.ctx, runtime.OpenDialogOptions{
		Title: "Select directory",
	})

	if err != nil {
		return "", err
	}

	return dir, nil
}
