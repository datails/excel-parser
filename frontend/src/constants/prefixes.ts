export enum Prefix {
  HISTORY = 'history',
  CONDITIONS = 'conditions',
  SETTINGS = 'settings',
}
