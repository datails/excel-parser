import { GridColDef, GridValueFormatterParams } from '@mui/x-data-grid'

export const historyColumns: GridColDef[] = [
  {
    field: 'id',
    description: 'Unique identifier of formatting job.',
    headerName: 'uuid',
    flex: 1
  },
  {
    field: 'createdAt',
    headerName: 'Created at',
    description: 'Creation date.',
    valueFormatter: (params: GridValueFormatterParams) => new Date(params.value).toLocaleString(),
    flex: 1,
  },
  {
    field: 'workbook',
    description: 'Path to the formatted Excel file.',
    headerName: 'Workbook',
    width: 260,
    flex: 1,
  },
  {
    field: 'worksheet',
    headerName: 'Worksheet',
    description: 'Name of worksheet.',
    flex: 1,
  },
  {
    field: 'delimiter',
    headerName: 'Delimiter',
    description: 'Selected delimiter.',
    flex: 1,
  },
]

export const rulesColumns: GridColDef[] = [
  {
    field: 'name',
    headerName: 'Name',
    description: 'Name of complex rule',
    flex: 1,
  },
  {
    field: 'createdAt',
    headerName: 'Created at',
    description: 'Creation date',
    valueFormatter: (params: GridValueFormatterParams) => new Date(params.value).toLocaleString(),
    flex: 1,
  },
  {
    field: 'rules',
    headerName: 'Rules',
    description: 'Applied rules',
    flex: 1,
  },
]
