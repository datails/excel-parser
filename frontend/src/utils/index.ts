export const isEmptyString = (str?: string) => str === '' || str === undefined
export const isDisabled = (...args: string[]) => args.some(isEmptyString)

export const parse = (data = '{}') => {
  const { key: id, value } = JSON.parse(data)

  return {
    id,
    ...JSON.parse(value || '{}'),
  }
}

export const isObject = (any?: unknown) => typeof any === 'object' && !Array.isArray(any) && any !== null

export const isEmptyObj = (obj?: unknown) => isObject(obj) && Object.keys(obj as Record<string, any>)?.length === 0

export const isEmptyArr = (arr?: any) => arr?.length === 0