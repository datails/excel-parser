import MuiTypography from '@mui/material/Typography'
import { styled } from '@mui/material/styles'
import withRoot from '@/theme/withRoot'

interface Props {
  children: any
}

const Typography = styled(MuiTypography)(({ theme }) => ({
  marginBottom: 35,
}))

function Header({ children }: Props) {
  return (
    <Typography className='title' variant='h1'>
      {children}
    </Typography>
  )
}

export default withRoot(Header)
