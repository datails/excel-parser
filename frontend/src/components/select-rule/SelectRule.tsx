import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Alert, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material'
import { List } from '@wails/go/main/App'
import withRoot from '@/theme/withRoot'
import ProgressBox from '../progress-box/ProgressBox'
import { Prefix } from '@/constants/prefixes'
import { Rule } from '@/types'
import { parse } from '@/utils'

interface Props {
  rule: Rule
  setRule: (evt: any) => void
}

function SelectRule({ rule, setRule }: Props) {
  const [rules, setRules] = useState<Rule[]>([])
  const [loading, setLoading] = useState(true)

  const handleChange = (event: SelectChangeEvent) => {
    setRule(rules.find(({ id }) => id === event.target.value))
  }

  useEffect(() => {
    const updateList = async () => {
      setLoading(true)

      List(Prefix.CONDITIONS)
        .then((data) => {
          setRules(data.map(parse))
        })
        .finally(() => {
          setLoading(false)
        })
    }

    updateList().catch(console.error)
  }, [])

  if (loading) {
    return <ProgressBox />
  }

  return (
    <>
      <InputLabel id='rule'>Select a rule</InputLabel>
      <Select
        id='rule'
        value={rule?.id}
        label='Select a rule'
        onChange={handleChange}
        labelId='rule'
      >
        {rules.map((rule, idx) => (
          <MenuItem key={idx} value={rule.id}>
            {rule.name}
          </MenuItem>
        ))}
      </Select>
      {rules.length === 0 && (
        <Alert severity='error'>
          There are no Rules configured yet. <Link to='/rules'>Please create one.</Link>
        </Alert>
      )}
    </>
  )
}

export default withRoot(SelectRule)
