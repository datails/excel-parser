import { useEffect, useState } from 'react'
import withRoot from '@/theme/withRoot'
import { List } from '@wails/go/main/App'
import ProgressBox from '@/components/progress-box/ProgressBox'
import DataTable from '@/components/data-table/DataTable'
import { Prefix } from '@/constants/prefixes'
import { GridColDef } from '@mui/x-data-grid'
import { parse } from '@/utils'

interface Props {
  prefix: Prefix
  columns: GridColDef[]
}

function BaderList({ columns, prefix }: Props) {
  const [rows, setRows] = useState([] as any[])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const updateList = async () => {
      setLoading(true)

      List(prefix)
        .then((data) => {
          setRows(data.map(parse))
        })
        .finally(() => {
          setLoading(false)
        })
    }

    updateList().catch(console.error)
  }, [])

  if (loading) {
    return <ProgressBox />
  }

  return (
    <>
      <DataTable rows={rows} columns={columns} />
    </>
  )
}

export default withRoot(BaderList)
