import { useContext } from 'react'
import { FormControl, TextField } from '@mui/material'
import { SelectDir } from '@wails/go/main/App'
import { SettingsContext } from '@/context/app.context'
import withRoot from '@/theme/withRoot'

function SelectDirectory() {
  const { settings, setSettings } = useContext(SettingsContext)

  function selectFile() {
    SelectDir()
      .then((directory) =>
        setSettings({
          ...settings,
          directory,
        }),
      )
      .catch(console.error)
  }

  return (
    <FormControl className='form-control'>
      <TextField
        value={settings.directory || ''}
        onClick={selectFile}
        label='Select a target directory.'
        id='directory'
        variant='outlined'
      />
    </FormControl>
  )
}

export default withRoot(SelectDirectory)
