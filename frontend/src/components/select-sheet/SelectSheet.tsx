import { useEffect, useState } from 'react'
import { InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material'
import { Worksheets } from '@wails/go/main/App'
import withRoot from '@/theme/withRoot'
import ProgressBox from '@/components/progress-box/ProgressBox'

interface Props {
  workbook: string
  worksheet: string
  setWorksheet: (worksheet: string) => void
}

function SelectSheet({ worksheet, setWorksheet, workbook }: Props) {
  const [worksheets, setWorksheets] = useState<string[]>(null as any)

  const handleChange = (event: SelectChangeEvent) => {
    setWorksheet(event.target.value as string)
  }

  useEffect(() => {
    Worksheets(workbook).then(setWorksheets)
  })

  if (worksheets === null) {
    return <ProgressBox />
  }

  return (
    <>
      <InputLabel id='worksheet'>Select a worksheet</InputLabel>
      <Select
        id='worksheet'
        value={worksheet}
        label='Select a worksheet'
        onChange={handleChange}
        labelId='worksheet'
      >
        {worksheets.map((sheet, idx) => (
          <MenuItem key={idx} value={sheet}>
            {sheet}
          </MenuItem>
        ))}
      </Select>
    </>
  )
}

export default withRoot(SelectSheet)
