import { useState } from 'react';
import Snackbar, { SnackbarOrigin } from '@mui/material/Snackbar';
import withRoot from '@/theme/withRoot';

export interface State extends SnackbarOrigin {
  open: boolean;
}

function AppSnackbar(msg: string) {
  const [state, setState] = useState<State>({
    open: true,
    vertical: 'bottom',
    horizontal: 'left',
  });
  const { vertical, horizontal, open } = state;

  const handleClose = () => {
    setState({ ...state, open: false });
  };

  return (
    <Snackbar
      anchorOrigin={{ vertical, horizontal }}
      open={open}
      onClose={handleClose}
      message={msg}
      key={vertical + horizontal}
    />
  );
}

export default withRoot(AppSnackbar)