import { useContext } from 'react'
import MuiBackdrop from '@mui/material/Backdrop'
import CircularProgress from '@mui/material/CircularProgress'
import withRoot from '@/theme/withRoot'
import { AppContext } from '@/context/app.context'

function Backdrop() {
  const { loading } = useContext(AppContext)

  return (
    <MuiBackdrop
      sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={loading}
    >
      <CircularProgress color='inherit' />
    </MuiBackdrop>
  )
}

export default withRoot(Backdrop)
