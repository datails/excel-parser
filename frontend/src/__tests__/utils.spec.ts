import { describe, test } from 'node:test'
import assert from 'node:assert'
import { isDisabled, isObject, isEmptyArr, isEmptyObj, isEmptyString } from '../utils/index'

describe('isEmptyString', () => {
  test('returns true when string is empty', () => {
    assert.strictEqual(isEmptyString(''), true)
  })

  test('returns true when string is undefined', () => {
    assert.strictEqual(isEmptyString(undefined), true)
  })

  test('returns false when string is not empty or undefined', () => {
    assert.strictEqual(isEmptyString('hello'), false)
  })
})

describe('isDisabled', () => {
  test('returns true when at least one argument is an empty string', () => {
    assert.strictEqual(isDisabled('', 'hello'), true)
  })

  test('returns false when all arguments are not empty strings or undefined', () => {
    assert.strictEqual(isDisabled('hello', 'world'), false)
  })
})

describe('isObject', () => {
  test('returns true when input is an object', () => {
    assert.strictEqual(isObject({}), true)
  })

  test('returns false when input is not an object', () => {
    assert.strictEqual(isObject([]), false)
    assert.strictEqual(isObject(null), false)
    assert.strictEqual(isObject(''), false)
  })
})

describe('isEmptyObj', () => {
  test('returns true when object is empty', () => {
    assert.strictEqual(isEmptyObj({}), true)
  })

  test('returns false when object is not empty', () => {
    assert.strictEqual(isEmptyObj({ key: 'value' }), false)
  })

  test('returns false when input is not an object', () => {
    assert.strictEqual(isEmptyObj([]), false)
    assert.strictEqual(isEmptyObj(null), false)
    assert.strictEqual(isEmptyObj(''), false)
  })
})

describe('isEmptyArr', () => {
  test('returns true when array is empty', () => {
    assert.strictEqual(isEmptyArr([]), true)
  })

  test('returns false when array is not empty', () => {
    assert.strictEqual(isEmptyArr([1, 2, 3]), false)
  })
})
