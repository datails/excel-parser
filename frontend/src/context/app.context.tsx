import React from 'react'

interface Settings {
  directory: string
  isHistoryEnabled: boolean
}

export const AppContext = React.createContext({
  loading: false,
  setLoading: (_bool: boolean) => {},
})

export const SettingsContext = React.createContext({
  settings: {} as Partial<Settings>,
  setSettings: (_settings: Partial<Settings>) => {},
})

export function AppContextProvider({ children }: any) {
  const [loading, setLoading] = React.useState(false)
  const [settings, setSettings] = React.useState({})

  const loadingValue = React.useMemo(
    () => ({
      loading,
      setLoading,
    }),
    [loading],
  )

  const settingsValue = React.useMemo(
    () => ({
      settings,
      setSettings,
    }),
    [settings, setSettings],
  )

  return (
    <AppContext.Provider value={loadingValue}>
      <SettingsContext.Provider value={settingsValue}>{children}</SettingsContext.Provider>
    </AppContext.Provider>
  )
}
