import '@fontsource/audiowide'
import 'react-js-cron/dist/styles.css'
import './style.css'
import './App.css'

import React from 'react'
import { HashRouter } from 'react-router-dom'
import { createRoot } from 'react-dom/client'
import { ThemeProvider } from '@emotion/react'
import { theme } from './theme/theme'
import App from './routes/app/App'
import { AppContextProvider } from './context/app.context'
import Backdrop from './components/backdrop/Backdrop'

const container = document.getElementById('root')
const root = createRoot(container!)

root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <AppContextProvider>
        <HashRouter>
          <Backdrop />
          <App />
        </HashRouter>
      </AppContextProvider>
    </ThemeProvider>
  </React.StrictMode>,
)
