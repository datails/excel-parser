import { useContext } from 'react'
import { AppContext } from '@/context/app.context'

type SetFunction = (...props: any[]) => Promise<any> | any

export const useLoader = (fn: SetFunction) => {
  const { setLoading } = useContext(AppContext)

  return {
    execute: async (...args:any[]) => {
      setLoading(true)

      const data = await fn(...args).catch(console.error)
      
      setLoading(false)

      return data
    }
  }
}