import { useState } from 'react'

export const useInputValue = (initialValue = '') => {
  const [inputValue, setInputValue] = useState(initialValue)

  return {
    inputValue,
    changeInput: (event: any) => setInputValue(event.target.value),
    clearInput: () => setInputValue(''),
    keyInput: (event: any, callback: any) => {
      if (event.which === 13 || event.keyCode === 13) {
        callback(inputValue)
        return true
      }
      return false
    },
  }
}