import { useState } from 'react'
import { isDisabled } from '@/utils'

export interface Rule {
  text: string
  checked: boolean
}

export const useRules = (initialValue = []) => {
  const [rules, setRules] = useState(initialValue as Rule[])

  return {
    rules,
    addRule: (rule: Omit<Rule, 'checked'>) => {
      if (!isDisabled(rule.text)) {
        setRules(
          rules.concat({
            ...rule,
            checked: false,
          }),
        )
      }
    },
    checkRule: (idx: number) => {
      setRules(
        rules.map((rule, index) => {
          if (idx === index) {
            rule.checked = !rule.checked
          }
          return rule
        }),
      )
    },
    removeRule: (idx: number) => {
      setRules(rules.filter((_, index) => idx !== index))
    },
    clearRules: () => setRules([]),
  }
}
