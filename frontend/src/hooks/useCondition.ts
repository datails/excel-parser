import { useState } from 'react'
import { isObject } from '@/utils'
import merge from 'ts-deepmerge'
import { nanoid } from 'nanoid'

export enum Condition {
  OR = 'OR',
  AND = 'AND',
}

export interface ICondition {
  id: string
  value: string
  condition?: Condition
  isFirst?: boolean
  children: Conditions
}

export interface Conditions {
  [index: string]: Partial<ICondition>
}

export const initialId = nanoid()

export const createDefaultCondition = () => ({
  [initialId]: {
    id: initialId,
    text: '',
    children: {},
  },
})

export const decodeDotNotatedPath = (str = '') => str.split('.').filter(Boolean)

export const getChildren = (obj: Record<string, any>, key: string) => isObject(obj?.children) ? obj.children[key] : obj[key]

export const deletePropAtPath = <T extends object>(path: string, obj: T) => {
  const keys = path.split('.');

  return keys.reduce((acc: any, key: string, index: number) => {
    if (index === keys.length - 1) {
      if (isObject(acc.children)) {
        delete acc.children[key]
        return true
      }

      delete acc[key];
      return true;
    }
    return isObject(acc?.children) ?
      acc.children[key] === Object(acc.children[key]) ?
        acc.children[key] :
        (acc.children[key] = {}) :
      acc[key] === Object(acc[key]) ?
        acc[key] :
        (acc[key] = {})
  }, obj);
}


export const createObjectFromPath = (str = '', obj: Record<string, any>) =>
  decodeDotNotatedPath(str)
    .reverse()
    .reduce(
      (acc, curr) => ({
        [curr]: {
          children: acc,
        },
      }),
      obj,
    ) 

export const setRootForOr = (dotNotatedPath = '') => {
  const paths = decodeDotNotatedPath(dotNotatedPath)

  paths.pop()

  if (decodeDotNotatedPath(dotNotatedPath).length === 0) {
    return nanoid()
  }

  return paths.length === 1 ? paths[0] : paths.join('.')
}

const createCondition = (condition: ICondition) => ({
  [condition.id]: {
    ...condition,
    children: {},
  },
})

const updateCondition = (condition: ICondition) => ({
  [condition.id]: {
    ...condition,
  },
})

export const useConditions = (initialValue: Conditions = createDefaultCondition()) => {
  const [conditions, setConditions] = useState(initialValue)

  return {
    conditions,
    createCondition: (condition: ICondition, dotNotatedPath?: string) => {
      if (condition.condition === Condition.OR) {
        setConditions(
          merge(
            conditions,
            // if dot notated path is empty,
            // it is a top level dot notated path
            !dotNotatedPath ? 
              createCondition(condition) :
                createObjectFromPath(setRootForOr(dotNotatedPath), {
                  [condition.id]: {
                    ...condition,
                    // is the first item in the object. We must know that for the conditions
                    isFirst: true,
                    children: {},
                  },
                }),
          ),
        )
      } else {
        setConditions(
          merge(
            conditions,
            createObjectFromPath(dotNotatedPath, {
              [condition.id]: condition,
            }),
          ),
        )
      }
    },
    setCondition: (condition: ICondition, dotNotatedPath?: string) => (evt: any) => {
      const newCondition = {
        ...condition,
        value: evt.target.value,
      }
      setConditions(merge(conditions, !dotNotatedPath ?
        updateCondition(newCondition) :
        createObjectFromPath(setRootForOr(dotNotatedPath), {
          [newCondition.id]: {
            ...newCondition,
            children: {},
          },
        })))
    },
    removeCondition: (dotNotatedPath: string) => {
      const newConds = { ...conditions }
      deletePropAtPath(dotNotatedPath, newConds)

      setConditions(newConds)
    },
    clearConditions: () => setConditions(createDefaultCondition()),
  }
}