import { Grid } from '@mui/material'
import withRoot from '@/theme/withRoot'
import { Prefix } from '@/constants/prefixes'
import BadgerTable from '@/components/bader-table/BadgerTable'
import { rulesColumns } from '@/constants/columns'

function DataGrid() {
  return (
    <Grid container spacing={2} justifyContent='center'>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <BadgerTable columns={rulesColumns} prefix={Prefix.CONDITIONS}></BadgerTable>
      </Grid>
    </Grid>
  )
}

export default withRoot(DataGrid)
