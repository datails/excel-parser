import { memo } from 'react'
import { Grid } from '@mui/material'
import withRoot from '@/theme/withRoot'
import { Conditions } from '@/hooks/useCondition'
import ConditionList from './ConditionList'
import { SetFunction } from '@/types'

interface Props {
  createCondition: SetFunction
  removeCondition: SetFunction
  setCondition: SetFunction
  conditions: Conditions
}

const Add = memo(function Add(props: Props) {
  return (
    <Grid container>
      <Grid xs={12} md={12} item style={{ paddingRight: 16 }}>
        <ConditionList
          createCondition={props.createCondition}
          removeCondition={props.removeCondition}
          conditions={props.conditions}
          setCondition={props.setCondition}
        />
      </Grid>
    </Grid>
  )
})

export default withRoot(Add)
