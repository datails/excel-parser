import { memo } from 'react'
import { TextField, Grid, IconButton, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import { ICondition, Condition } from '@/hooks/useCondition'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline'
import ClearAllIcon from '@mui/icons-material/ClearAll'
import { DeleteOutline } from '@mui/icons-material'
import { nanoid } from 'nanoid'
import { SetFunction } from '@/types'

interface Props {
  setCondition: any
  createCondition: SetFunction
  removeCondition: SetFunction
  condition: ICondition
  key: string
  idx: number
}

const Conditional = memo(function Conditional(props: Props) {
  return (
    <Grid container>
      <Grid xs={10} md={11} item style={{ paddingRight: 16 }}>
        {props.idx !== 0 && <Typography>{props.condition.condition}</Typography>}
        <TextField 
          autoComplete='off' 
          autoCapitalize='off' 
          autoCorrect='off' 
          placeholder='Delimiter' 
          value={props.condition.value} 
          onChange={props.setCondition(props.condition, props.idx)} 
          fullWidth />
        {props.condition.value && (
          <>
            <IconButton
              color='primary'
              className='conditional-button'
              onClick={() =>
                props.createCondition(
                  {
                    id: nanoid(),
                    condition: Condition.AND,
                    text: '',
                  },
                  props.idx,
                )
              }
            >
              <AddCircleOutlineIcon />
            </IconButton>
            <IconButton
              color='primary'
              className='conditional-button'
              onClick={() =>
                props.createCondition(
                  {
                    id: nanoid(),
                    condition: Condition.OR,
                    text: '',
                  },
                  props.idx,
                )
              }
            >
              <ClearAllIcon />
            </IconButton>
          </>
        )}
        {props.idx !== 0 && (
          <IconButton onClick={() => props.removeCondition(props.idx)}>
            <DeleteOutline />
          </IconButton>
        )}
      </Grid>
    </Grid>
  )
})
export default withRoot(Conditional)
