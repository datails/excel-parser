import { memo } from 'react'
import { List, Paper } from '@mui/material'
import ListItem from './ListItem'
import withRoot from '@/theme/withRoot'
import { Rule } from '@/hooks/useRules'

interface Props {
  rules: Rule[]
  onItemRemove: any
  onItemCheck: any
}

const RuleList = memo(function RuleList(props: Props){ 
  return (
  <>
    {props.rules.length > 0 && (
      <Paper className='margin16'>
        <List style={{ overflow: 'scroll' }}>
          {props.rules.map((rule: Rule, id: number) => (
            <>
              <ListItem
                {...rule}
                key={`ListItem.${id}`}
                divider={id !== props.rules.length - 1}
                onButtonClick={() => props.onItemRemove(id)}
                onCheckBoxToggle={() => props.onItemCheck(id)}
              />
            </>
          ))}
        </List>
      </Paper>
    )}
  </>
)})

export default withRoot(RuleList)
