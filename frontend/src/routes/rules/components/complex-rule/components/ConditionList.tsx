import { memo } from 'react'
import withRoot from '@/theme/withRoot'
import { Conditions } from '@/hooks/useCondition'
import Conditional from './Conditional'
import { SetFunction } from '@/types'

interface Props {
  createCondition: SetFunction
  removeCondition: SetFunction
  setCondition: SetFunction
  conditions: Conditions
  groupId: string
}

const isTopLevelOr = (oldKey: string, newKey: string) => oldKey === newKey || oldKey === undefined

const createDotNotatedKey = (oldKey: string, newKey: string) => {  
  return isTopLevelOr(oldKey, newKey) ? newKey : `${oldKey}.${newKey}`
}

const ConditionList = memo(function ConditionList(props: Props) {
  return (
    <div className='condition-group' data-groupId={props.groupId}>
      {Object.entries(props.conditions).map(([key, value]) => (
        <>
          <Conditional
            key={value.id}
            condition={value}
            removeCondition={props.removeCondition}
            idx={createDotNotatedKey(props.groupId, key)}
            createCondition={props.createCondition}
            setCondition={props.setCondition}
          />
          {Object.keys(value?.children || {}).length !== 0 && (
            <ConditionList
              {...props}
              conditions={value.children as Conditions}
              groupId={createDotNotatedKey(props.groupId, key)}
            />
          )}
        </>
      ))}
    </div>
  )
})
export default withRoot(ConditionList)
