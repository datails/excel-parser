import { memo } from 'react'
import {
  ListItem,
  Checkbox,
  IconButton,
  ListItemText,
  ListItemSecondaryAction,
} from '@mui/material'
import DeleteOutlined from '@mui/icons-material/DeleteOutlined'
import withRoot from '@/theme/withRoot'

interface Props {
  divider: any
  onCheckBoxToggle: any
  checked: any
  text: any
  onButtonClick: any
}

const RuleListItem = memo(function RuleListItem(props: Props) {
  return (
  <ListItem divider={props.divider}>
    <Checkbox onClick={props.onCheckBoxToggle} checked={props.checked} disableRipple />
    <ListItemText primary={props.text} />
    <ListItemSecondaryAction>
      <IconButton aria-label='Delete Todo' onClick={props.onButtonClick}>
        <DeleteOutlined />
      </IconButton>
    </ListItemSecondaryAction>
  </ListItem>
)})

export default withRoot(RuleListItem)
