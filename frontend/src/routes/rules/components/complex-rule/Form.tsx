import { memo, useState } from 'react'
import classnames from 'classnames'
import SaveIcon from '@mui/icons-material/Save'
import withRoot from '@/theme/withRoot'
import { Button, Grid, TextField } from '@mui/material'
import { Stack } from '@mui/system'
import Add from './components/Add'
import { Conditions, useConditions } from '@/hooks/useCondition'
import { Prefix } from '@/constants/prefixes'
import { SetItem } from '@wails/go/main/App'
import { useLoader } from '@/hooks/useLoader'

const isDisabled = (conditions: Conditions, name: string) => Object.keys(conditions).length === 0 || name === ''

const RulesForm = memo(function RulesForm() {
  const [rule, setRule] = useState<string>('')
  const { execute } = useLoader(SetItem)

  const { conditions, createCondition, removeCondition, setCondition, clearConditions } = useConditions()

  const handleOnChange = (e: any) => {
    setRule(e.target.value)
  }

  const handleAddRules = () => {
    execute(Prefix.CONDITIONS, JSON.stringify({
      name: rule,
      conditions,
      createdAt: Date.now(),
    })).then(() => {
      clearConditions();
      setRule('');
    })
  }

  return (
    <Grid container>
      <Grid
        xs={12}
        sm={8}
        lg={6}
        className={classnames('flex', 'justifyStart', 'flexDirectionColumn')}
      >
        <TextField
          className='margin16'
          variant='outlined'
          onChange={handleOnChange}
          value={rule}
          label='Complex rule name'
        ></TextField>
        <Add 
          conditions={conditions}
          createCondition={createCondition}
          removeCondition={removeCondition}
          setCondition={setCondition}
        />
        <Stack spacing={2} className='input-box'>
          <Button
            disabled={isDisabled(conditions, rule)}
            onClick={handleAddRules}
            className='button form'
            variant='outlined'
            startIcon={<SaveIcon />}
          >
            Save
          </Button>
        </Stack>
      </Grid>
    </Grid>
  )
})

export default withRoot(RulesForm)
