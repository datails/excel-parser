import classNames from 'classnames'
import { Card, CardContent, Divider, Grid, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import DataGrid from './components/DataGrid'
import RulesForm from './components/complex-rule/Form'

function Rules() {
  return (
    <Grid container spacing={2} id='app' justifyContent='center'>
      <Grid item xs={12} flexDirection='column' className={classNames('marginBottom32 title-bar')}>
        <Card>
          <CardContent>
            <Typography
              className='margin16 marginBottom32'
              gutterBottom
              variant='h1'
              component='div'
            >
              Create Conditionals
            </Typography>
            <Typography component={'body'}>
              A conditional can be used to define sequential steps in order to format the Excel
              file.
              <br />
              It is particular useful, when you want to run multiple delimiters sequantially.
            </Typography>
            <Divider className='divide'></Divider>
            <RulesForm />
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <Card>
          <CardContent>
            <Typography
              className='margin16 marginBottom32'
              gutterBottom
              variant='h1'
              component='div'
            >
              Conditionals
            </Typography>
            <Typography component={'body'}>List view of created conditionals.</Typography>
            <Divider className='divide'></Divider>
            <DataGrid />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default withRoot(Rules)
