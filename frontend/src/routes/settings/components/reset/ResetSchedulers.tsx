import { useState } from 'react'
import { Button } from '@mui/material'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import withRoot from '@/theme/withRoot'
import Dialog from './components/Dialog'
import { ClearAllSchedulers } from '@wails/go/main/App'

function ResetSchedulers() {
  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <>
      <Button variant='outlined' onClick={handleOpen} startIcon={<DeleteForeverIcon />}>
        Reset schedulers
      </Button>
      <Dialog prefix={'schedulers'} resetFn={ClearAllSchedulers} open={open} setOpen={setOpen} />
    </>
  )
}

export default withRoot(ResetSchedulers)
