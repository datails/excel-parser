import { useState } from 'react'
import { Button } from '@mui/material'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import withRoot from '@/theme/withRoot'
import Dialog from './components/Dialog'
import { Prefix } from '@/constants/prefixes'
import { ResetPrefix } from '@wails/go/main/App'

interface Props {
  prefix: Prefix
}

function ResetAllPrefix({ prefix }: Props) {
  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const resetFn = () => ResetPrefix(prefix)

  return (
    <>
      <Button variant='outlined' onClick={handleOpen} startIcon={<DeleteForeverIcon />}>
        Reset {prefix}
      </Button>
      <Dialog prefix={prefix} resetFn={resetFn} open={open} setOpen={setOpen} />
    </>
  )
}

export default withRoot(ResetAllPrefix)
