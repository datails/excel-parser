import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import withRoot from '@/theme/withRoot'
import { SetFunction } from '@/types'
import { useLoader } from '@/hooks/useLoader'

interface Props {
  open: boolean
  setOpen: SetFunction
  resetFn: SetFunction
  prefix?: string
}

function AlertDialog({ open, setOpen, resetFn, prefix = 'all' }: Props) {
  const { execute } = useLoader(resetFn)

  const handleReset = () => execute().then(() => setOpen(false))

  const handleClose = () => setOpen(false)

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
    >
      <DialogTitle id='alert-dialog-title'>{'Delete all data'}</DialogTitle>
      <DialogContent>
        <DialogContentText id='alert-dialog-description'>
          Are you sure, you want to reset {prefix} application data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleClose}>
          No
        </Button>
        <Button onClick={handleReset}>Yes</Button>
      </DialogActions>
    </Dialog>
  )
}

export default withRoot(AlertDialog)
