import { useContext } from 'react'
import { FormControl, Button, Switch, FormControlLabel } from '@mui/material'
import { Set } from '@wails/go/main/App'
import { Stack } from '@mui/system'
import withRoot from '@/theme/withRoot'
import { Prefix } from '@/constants/prefixes'
import SelectDirectory from '@/components/select-dir/SelectDir'
import { SettingsContext } from '@/context/app.context'
import { useLoader } from '@/hooks/useLoader'

function Form() {
  const { settings, setSettings } = useContext(SettingsContext)
  const { execute } = useLoader(Set)

  const save = () => execute(
    Prefix.SETTINGS,
    JSON.stringify({
      updatedAt: Date.now(),
      ...settings,
    })
  )

  const handleSwitch = () =>
    setSettings({
      ...settings,
      isHistoryEnabled: !settings.isHistoryEnabled,
    })

  return (
    <>
      <SelectDirectory />
      <FormControl className='form-control'>
        <FormControlLabel
          control={
            <Switch
              checked={settings.isHistoryEnabled || false}
              value={settings.isHistoryEnabled || false}
              onClick={handleSwitch}
            />
          }
          label='Enable saving history'
        />
      </FormControl>
      <Stack spacing={2} className='input-box'>
        <Button onClick={save} className='button form' variant='outlined'>
          Save
        </Button>
      </Stack>
    </>
  )
}

export default withRoot(Form)
