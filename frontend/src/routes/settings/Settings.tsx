import { Card, CardContent, Grid, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import Form from './components/Form'
import Reset from './components/reset/Reset'
import ResetPrefix from './components/reset/ResetPrefix'
import { Prefix } from '@/constants/prefixes'
import ResetSchedulers from './components/reset/ResetSchedulers'

function Settings() {
  return (
    <Grid container spacing={2} id='app' justifyContent='center'>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <Card>
          <CardContent>
            <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
              Settings
            </Typography>
            <Typography className='title marginBottom32' variant='h5'>
              Configure global application settings.
            </Typography>
            <Form />
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <Card>
          <CardContent>
            <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
              Reset
            </Typography>
            <Typography className='title marginBottom32' variant='h5'>
              Reset all configs, data and personal settings.
            </Typography>
            <Reset />
            <ResetSchedulers />
            <ResetPrefix prefix={Prefix.HISTORY} />
            <ResetPrefix prefix={Prefix.CONDITIONS} />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default withRoot(Settings)
