import { Grid } from '@mui/material'
import withRoot from '@/theme/withRoot'
import { Prefix } from '@/constants/prefixes'
import BadgerTable from '@/components/bader-table/BadgerTable'
import { historyColumns } from '@/constants/columns'

function DataGrid() {
  return (
    <Grid container spacing={2} justifyContent='center'>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <BadgerTable columns={historyColumns} prefix={Prefix.HISTORY}></BadgerTable>
      </Grid>
    </Grid>
  )
}

export default withRoot(DataGrid)
