import { Routes, Route } from 'react-router-dom'
import Home from './home/Home'
import SimpleFormat from './simple-format/SimpleFormat'
import History from './history/History'
import Rules from './rules/Rules'
import ComplexFormat from './complex-format/ComplexFormat'
import Settings from './settings/Settings'
import Schedulers from './schedulers/Schedulers'

const Router = () => {
  return (
    <>
      <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='/simple-format' element={<SimpleFormat />}></Route>
        <Route path='/complex-format' element={<ComplexFormat />}></Route>
        <Route path='/history' element={<History />}></Route>
        <Route path='/rules' element={<Rules />}></Route>
        <Route path='/settings' element={<Settings />}></Route>
        <Route path='/schedulers' element={<Schedulers />}></Route>
      </Routes>
    </>
  )
}

export default Router
