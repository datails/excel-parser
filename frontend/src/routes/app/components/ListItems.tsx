import HomeIcon from '@mui/icons-material/Home'
import DescriptionIcon from '@mui/icons-material/Description'
import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import HistoryIcon from '@mui/icons-material/History'
import SettingsIcon from '@mui/icons-material/Settings'

export const listItems = [
  {
    key: 'Overview',
    icon: <HomeIcon />,
    href: '#/',
  },
  {
    key: 'Simple Format',
    icon: <DescriptionIcon />,
    href: '#/simple-format',
  },
  {
    key: 'Complex Format',
    icon: <ContentPasteSearchIcon />,
    href: '#/complex-format',
  },
  {
    key: 'Conditionals',
    icon: <FormatAlignCenterIcon />,
    href: '#/rules',
  },
  // TO DO: figure out if this is even possible
  // {
  //   key: 'Schedulers',
  //   icon: <MoreTimeIcon />,
  //   href: '#/schedulers',
  // },
  // {
  //   key: 'CSV parser',
  //   icon: <FormatAlignCenterIcon />,
  //   href: '#/csv-parser',
  // },
  {
    key: 'History',
    icon: <HistoryIcon />,
    href: '#/history',
  },
  {
    key: 'Settings',
    icon: <SettingsIcon />,
    href: '#/settings',
  },
]
