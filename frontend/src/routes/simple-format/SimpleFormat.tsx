import { Card, CardContent, Grid } from '@mui/material'
import withRoot from '@/theme/withRoot'

import Form from './components/Form'
import Header from './components/Header'
import { SimpleFormatCtxProvider } from './simple-format.context'

function SimpleFormatForm() {
  return (
    <SimpleFormatCtxProvider>
      <Grid container spacing={2} id='app' justifyContent='center'>
        <Grid item xs={12} className='title-bar' flexDirection='column'>
          <Card>
            <CardContent>
              <Header />
              <Form />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </SimpleFormatCtxProvider>
  )
}

export default withRoot(SimpleFormatForm)
