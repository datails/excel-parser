import MuiTypography from '@mui/material/Typography'
import { styled } from '@mui/material/styles'
import withRoot from '@/theme/withRoot'

const Typography = styled(MuiTypography)(({ theme }) => ({
  marginBottom: 35,
}))

function Header() {
  return (
    <>
      <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
        Simple Format
      </Typography>
      <Typography className='title' variant='h5'>
        Easily formats an Excel worksheet using a single delimiter. Simply select a workbook,
        worksheet and a delimiter and queue the file to be formatted.
      </Typography>
    </>
  )
}

export default withRoot(Header)
