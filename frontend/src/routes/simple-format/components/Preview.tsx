import { ChangeEvent, useContext, useState } from 'react'
import {
  Button,
  Table,
  TableRow,
  TableCell,
  TableBody,
  FormControl,
  TextField,
  Typography,
} from '@mui/material'
import { ReadFile, SetItem, FormatSheet, FormatExcel } from '@wails/go/main/App'
import { Stack } from '@mui/system'
import withRoot from '@/theme/withRoot'
import ProgressBox from '@/components/progress-box/ProgressBox'
import { Prefix } from '@/constants/prefixes'
import { SettingsContext } from '@/context/app.context'
import { useNavigate } from 'react-router-dom'
import { useLoader } from '@/hooks/useLoader'
import { useApi } from '@/hooks/useApi'
import { isEmptyArr } from '@/utils'
import Snackbar from '@/components/snackbar/Snackbar'

interface Props {
  worksheet: string
  workbook: string
  prefix: Prefix
}

function Preview({ prefix, worksheet, workbook }: Props) {
  const [rows, setRows] = useState<string[][]>([])
  const [delimiter, setDelimiter] = useState<string>(';')
  const { execute: executeFormatSheet } = useLoader(FormatSheet)
  const { execute: executeSetItem } = useLoader(SetItem)
  const { execute: executeFormatExcel } = useLoader(FormatExcel)
  const { data, error } = useApi(ReadFile, workbook, worksheet)
  const { settings } = useContext(SettingsContext)
  const navigate = useNavigate()

  const handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    if (event.target.value === '') {
      setDelimiter(event.target.value)
      return
    } 

    executeFormatSheet(
      JSON.stringify({
        createdAt: Date.now(),
        worksheet,
        workbook,
        target: settings.directory,
        // the delimiter will always be in an array
        // for maximum flexibility
        delimiter: [event.target.value],
      }),
    ).then((e) => {
      setDelimiter(event.target.value)
      setRows([...e.Rows])
    })
  }

  const addHistory = () =>
    executeSetItem(
      prefix,
      JSON.stringify({
        createdAt: Date.now(),
        worksheet,
        workbook,
        delimiter: [delimiter],
        target: settings.directory,
      }),
    ).finally(() => {
      navigate('/')
    })

  const applyFormatting = () => executeFormatExcel(JSON.stringify({
    createdAt: Date.now(),
    worksheet,
    workbook,
    delimiter: [delimiter],
    target: settings.directory,
  })).then(addHistory)

  if (error) {
    return <Snackbar error={error.message} />
  }

  if (data === undefined) {
    return <ProgressBox />

  }

  if (isEmptyArr(data)) {
    return (<Typography>The selected Worksheet is empty!</Typography>)
  }

  return (
    <>
      <FormControl className='form-control'>
        <TextField
          onChange={handleChange}
          defaultValue={delimiter}
          id='delimiter'
          label='Delimiter'
          variant='outlined'
        />
      </FormControl>
      <Table>
        <TableBody>
          {(rows.length === 0 ? data : rows).map(
            (row: any, idx: number) =>
              Array.isArray(row) && (
                <TableRow key={idx}>
                  {row.map((col, idx) => (
                    <TableCell key={idx} align='right'>
                      {col}
                    </TableCell>
                  ))}
                </TableRow>
              ),
          )}
        </TableBody>
      </Table>
      <Stack spacing={2} className='input-box'>
        <Button onClick={applyFormatting} className='button form' variant='outlined'>
          Apply formatting
        </Button>
      </Stack>
    </>
  )
}

export default withRoot(Preview)
