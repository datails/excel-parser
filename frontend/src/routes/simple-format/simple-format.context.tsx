import React from 'react'

export const WorkbookContext = React.createContext({
  workbook: '',
  setWorkbook: (_str: string) => {},
})

export const WorksheetContext = React.createContext({
  worksheet: '',
  setWorksheet: (_str: string) => {},
})

export const DelimiterContext = React.createContext({
  delimiter: ';',
  setDelimiter: (_str: string) => {},
})

export const WorksheetsContext = React.createContext({
  worksheets: [] as string[],
  setWorksheets: (_arr: string[]) => {},
})

export const RowsContext = React.createContext({
  rows: [] as string[][],
  setRows: (_arr: string[][]) => {},
})

export function SimpleFormatCtxProvider({ children }: any) {
  const [workbook, setWorkbook] = React.useState('')
  const [worksheet, setWorksheet] = React.useState('')
  const [worksheets, setWorksheets] = React.useState<string[]>([])
  const [rows, setRows] = React.useState<string[][]>([])

  const workbookValue = React.useMemo(
    () => ({
      workbook,
      setWorkbook,
    }),
    [workbook],
  )

  const worksheetValue = React.useMemo(
    () => ({
      worksheet,
      setWorksheet,
    }),
    [worksheet],
  )

  const worksheetsValue = React.useMemo(
    () => ({
      worksheets,
      setWorksheets,
    }),
    [worksheets],
  )

  const rowsValue = React.useMemo(
    () => ({
      rows,
      setRows,
    }),
    [rows],
  )

  return (
    <WorkbookContext.Provider value={workbookValue}>
      <WorksheetContext.Provider value={worksheetValue}>
        <WorksheetsContext.Provider value={worksheetsValue}>
          <RowsContext.Provider value={rowsValue}>{children}</RowsContext.Provider>
        </WorksheetsContext.Provider>
      </WorksheetContext.Provider>
    </WorkbookContext.Provider>
  )
}
