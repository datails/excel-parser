import React, { useEffect, useState } from 'react'
import { styled } from '@mui/material/styles'
import { Alert, Grid, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import Logo from '@/assets/images/logo_transparent.png'
import { List } from '@wails/go/main/App'
import { Prefix } from '@/constants/prefixes'
import { parse } from '@/utils'
import ProgressBox from '@/components/progress-box/ProgressBox'

const AppLogo = styled('img')(({ theme }) => ({
  width: 256,
}))

const sortByDate = (a: any, b: any) =>
  new Date(b.createdAt).valueOf() - new Date(a.createdAt).valueOf()

function Home() {
  const [rows, setRows] = useState([] as any[])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const updateList = async () => {
      setLoading(true)

      List(Prefix.HISTORY)
        .then((data) => {
          setRows(data.map(parse))
        })
        .finally(() => {
          setLoading(false)
        })
    }

    updateList().catch(console.error)
  }, [])

  if (loading) {
    return <ProgressBox />
  }

  return (
    <Grid container spacing={2} id='app' justifyContent='center'>
      <Grid item xs={8} flexDirection='column' className='title-bar'>
        <AppLogo src={Logo} alt='Logo' />
        <Typography className='title marginBottom32' component={'h1'}>
          Move faster with intuitive Excel formatting tools. Get started now.
        </Typography>
        {rows
          .sort(sortByDate)
          .slice(0, 5)
          .map((row, idx) => (
            <Alert key={idx} className='margin8' severity='info'>
              Parsed worbook {row.workbook} at {new Date(row.createdAt).toISOString()}
            </Alert>
          ))}
      </Grid>
    </Grid>
  )
}

export default withRoot(Home)
