import { Button, FormControl, TextField } from '@mui/material'
import { Stack } from '@mui/system'
import { useContext, useState } from 'react'
import { Cron } from 'react-js-cron'
import { useNavigate } from 'react-router-dom'
import { Schedule, SelectWorkBook } from '@wails/go/main/App'
import SelectRule from '@/components/select-rule/SelectRule'
import SelectSheet from '@/components/select-sheet/SelectSheet'
import { SettingsContext } from '@/context/app.context'
import withRoot from '@/theme/withRoot'
import { Rule } from '@/types'
import { isDisabled, isEmptyString } from '@/utils'

function SchedulerForm() {
  const [value, setValue] = useState('30 5 * * 1,6')
  const [workbook, setWorkbook] = useState('')
  const [worksheet, setWorksheet] = useState('')
  const [rule, setRule] = useState<Rule>(null as any)
  const { settings } = useContext(SettingsContext)

  const navigate = useNavigate()

  function selectFile() {
    SelectWorkBook().then(setWorkbook).catch(console.error)
  }

  function addJob() {
    Schedule(
      value,
      JSON.stringify({
        workbook,
        worksheet,
        target: settings.directory,
        delimiter: rule.rules,
      }),
    ).then(() => navigate('/'))
  }

  return (
    <>
      <FormControl className='form-control'>
        <TextField
          value={workbook || ''}
          onClick={selectFile}
          label='Select a file.'
          id='workbook'
          variant='outlined'
        />
      </FormControl>
      {!isEmptyString(workbook) && (
        <>
          <FormControl className='form-control'>
            <SelectSheet
              worksheet={worksheet}
              setWorksheet={setWorksheet}
              workbook={workbook}
            ></SelectSheet>
          </FormControl>
          <FormControl className='form-control'>
            <SelectRule setRule={setRule} rule={rule} />
          </FormControl>
        </>
      )}
      <FormControl>
        <Cron value={value} setValue={setValue} />
      </FormControl>
      <Stack spacing={2} className='input-box'>
        <Button
          disabled={isEmptyString(rule?.id) && isDisabled(workbook, value, rule?.id, worksheet)}
          onClick={addJob}
          className='button form'
          variant='outlined'
        >
          Schedule
        </Button>
      </Stack>
    </>
  )
}

export default withRoot(SchedulerForm)
