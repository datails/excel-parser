import { Card, CardContent, Grid, Typography } from '@mui/material'
import withRoot from '@/theme/withRoot'
import DataGrid from './components/DataGrid'
import Scheduler from './components/Form'

function Schedulers() {
  return (
    <Grid container spacing={2} id='app' justifyContent='center'>
      <Grid item xs={12} flexDirection='column' className='title-bar'>
        <Card>
          <CardContent>
            <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
              Schedulers
            </Typography>
            <Typography className='title marginBottom32' variant='h5'>
              Schedule when specific files should be formatted.
            </Typography>
            <Scheduler />
            <DataGrid />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default withRoot(Schedulers)
