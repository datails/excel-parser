import { Card, CardContent, Grid } from '@mui/material'
import withRoot from '@/theme/withRoot'

import Form from './components/Form'
import Header from './components/Header'
import { ComplexFormatCtxProvider } from './complex-format.context'

function ComplexFormat() {
  return (
    <ComplexFormatCtxProvider>
      <Grid container spacing={2} id='app' justifyContent='center'>
        <Grid item xs={12} className='title-bar' flexDirection='column'>
          <Card>
            <CardContent>
              <Header />
              <Form />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </ComplexFormatCtxProvider>
  )
}

export default withRoot(ComplexFormat)
