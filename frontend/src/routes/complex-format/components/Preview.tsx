import { useContext, useState } from 'react'
import { Button, Table, TableRow, TableCell, TableBody, Typography } from '@mui/material'
import { ReadFile, SetItem, ComplexFormatSheet, FormatComplexExcel } from '@wails/go/main/App'
import { Stack } from '@mui/system'
import withRoot from '@/theme/withRoot'
import ProgressBox from '@/components/progress-box/ProgressBox'
import { Prefix } from '@/constants/prefixes'
import SelectRule from '@/components/select-rule/SelectRule'
import { SettingsContext } from '@/context/app.context'
import { marshallConditions } from './utils/conditions'
import { Conditions } from '@/hooks/useCondition'
import { useApi } from '@/hooks/useApi'
import Snackbar from '@/components/snackbar/Snackbar'
import { useLoader } from '@/hooks/useLoader'
import { isEmptyArr } from '@/utils'

interface Props {
  worksheet: string
  workbook: string
  prefix: Prefix
}

interface SavedCondition {
  name: string, 
  conditions: Conditions, 
  id: string
}

const isCurrent = (oldRule?: SavedCondition, newRule?: SavedCondition) => 
  oldRule?.id === newRule?.id

function Preview({ prefix, worksheet, workbook }: Props) {
  const [rows, setRows] = useState<string[][]>([])
  const [rule, setRule] = useState<SavedCondition>(null as any)
  const { settings } = useContext(SettingsContext)
  const { data, error } = useApi(ReadFile, workbook, worksheet)
  const { execute } = useLoader(SetItem)
  const { execute: executeComplexFormat } = useLoader(ComplexFormatSheet)
  const { execute: executeFormatExcel } = useLoader(FormatComplexExcel)

  const handleChange = (newRule: SavedCondition) => {
    if (isCurrent(rule, newRule)) return

    executeComplexFormat(JSON.stringify({
      createdAt: Date.now(),
      worksheet,
      workbook,
      delimiter: (marshallConditions(newRule.conditions) || []),
    })).then((e) => {
      setRule(newRule)
      setRows([...e.Rows])
    })
  }

  const addHistory = () =>
    execute(prefix, JSON.stringify({
      createdAt: Date.now(),
      worksheet,
      workbook,
      delimiter: (marshallConditions(rule.conditions) || []),
      target: settings.directory,
    }))

  const applyFormatting = () => executeFormatExcel(JSON.stringify({
    createdAt: Date.now(),
    worksheet,
    workbook,
    delimiter: (marshallConditions(rule.conditions) || []),
    target: settings.directory,
  })).then(addHistory)

  if (error) {
    return <Snackbar error={error.message} />
  }

  if (data === undefined && rule === null) {
    return <ProgressBox />
  }

  if (isEmptyArr(data)) {
    return (<Typography>The selected Worksheet is empty!</Typography>)
  }

  return (
    <>
      <SelectRule setRule={handleChange} rule={rule} />
      <Table>
        <TableBody>
          {rows.map(
            (row, idx) =>
              Array.isArray(row) && (
                <TableRow key={idx}>
                  {row.map((col, idx2) => (
                    <TableCell key={idx2} align='right'>
                      {col}
                    </TableCell>
                  ))}
                </TableRow>
              ),
          )}
        </TableBody>
      </Table>
      <Stack spacing={2} className='input-box'>
        <Button onClick={applyFormatting} className='button form' variant='outlined'>
          Apply formatting
        </Button>
      </Stack>
    </>
  )
}

export default withRoot(Preview)
