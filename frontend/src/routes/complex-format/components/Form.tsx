import { useContext, useState } from 'react'
import { FormControl, TextField, Button } from '@mui/material'
import { SelectWorkBook } from '@wails/go/main/App'
import { Stack } from '@mui/system'
import withRoot from '@/theme/withRoot'
import Preview from './Preview'
import { WorkbookContext, WorksheetContext } from '../complex-format.context'
import { Prefix } from '@/constants/prefixes'
import { isDisabled, isEmptyString } from '@/utils'
import SelectSheet from '@/components/select-sheet/SelectSheet'
import SelectDir from '@/components/select-dir/SelectDir'

function Form() {
  const { workbook, setWorkbook } = useContext(WorkbookContext)
  const { worksheet, setWorksheet } = useContext(WorksheetContext)
  const [preview, setPreview] = useState<boolean>(false)

  const togglePreview = () => setPreview(!preview)

  function selectFile() {
    SelectWorkBook().then(setWorkbook).catch(console.error)
  }

  if (preview && !isEmptyString(workbook) && !isEmptyString(worksheet)) {
    return <Preview prefix={Prefix.HISTORY} worksheet={worksheet} workbook={workbook} />
  }

  return (
    <>
      <FormControl className='form-control'>
        <TextField
          value={workbook || ''}
          onClick={selectFile}
          label='Select a file.'
          id='workbook'
          variant='outlined'
        />
      </FormControl>
      {!isEmptyString(workbook) && (
        <>
          <FormControl className='form-control'>
            <SelectSheet
              worksheet={worksheet}
              setWorksheet={setWorksheet}
              workbook={workbook}
            ></SelectSheet>
          </FormControl>
        </>
      )}
      <SelectDir />
      <Stack spacing={2} className='input-box'>
        <Button
          disabled={isDisabled(workbook, worksheet)}
          onClick={togglePreview}
          className='button form'
          variant='outlined'
        >
          Preview formatting
        </Button>
      </Stack>
    </>
  )
}

export default withRoot(Form)
