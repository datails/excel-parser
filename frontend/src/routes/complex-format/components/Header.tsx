import MuiTypography from '@mui/material/Typography'
import { styled } from '@mui/material/styles'
import withRoot from '@/theme/withRoot'

const Typography = styled(MuiTypography)(({ theme }) => ({
  marginBottom: 35,
}))

function Header() {
  return (
    <>
      <Typography className='margin16 marginBottom32' gutterBottom variant='h1'>
        Complex Format
      </Typography>
      <Typography className='title' variant={'h5'}>
        Easily formats an Excel worksheet using predefined delimiters.
      </Typography>
    </>
  )
}

export default withRoot(Header)
