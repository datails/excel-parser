import { Conditions, ICondition } from '@/hooks/useCondition';

export const isEmpty = (obj: Record<string, any> = {}) => Object.keys(obj).length === 0

export const marshallConditions = (cond: Conditions = {}): any[] => 
  Object.entries(cond || {}).reduce((
    acc: any[], [, value]: [string, Partial<ICondition>]): any[] => {
      if (!isEmpty(value.children)) {
        return [
          ...acc, 
          {
            ...value,
            children: marshallConditions(value.children)
          },
        ]
      }

    return [
      ...acc,
      {
        ...value,
        children: [],
      },
    ]
  }, [])
