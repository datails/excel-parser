import React from 'react'
import CssBaseline from '@mui/material/CssBaseline'
import { theme } from './theme'
import { ThemeProvider } from '@emotion/react'

export default function withRoot(Component: React.ComponentType<any>) {
  function WithRoot(props: Record<string, unknown>) {
    return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...props} />
      </ThemeProvider>
    )
  }

  return WithRoot
}
