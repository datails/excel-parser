import React from 'react'
import { Link as RouterLink, LinkProps as RouterLinkProps } from 'react-router-dom'
import { LinkProps } from '@mui/material/Link'
import { createTheme } from '@mui/material/styles'

const LinkBehavior = React.forwardRef<
  HTMLAnchorElement,
  Omit<RouterLinkProps, 'to'> & { href: RouterLinkProps['to'] }
>(function LinkBehavior(props, ref) {
  const { href, ...other } = props
  return <RouterLink ref={ref} to={href} {...other} />
})

export const rawTheme = createTheme({
  palette: {
    primary: {
      light: '#9DCFCA',
      main: '#93c2bd',
      dark: '#324241',
    },
    secondary: {
      light: '#CFB2B2',
      main: '#C2A7A7',
      dark: '#423939',
    },
  },
  components: {
    MuiLink: {
      defaultProps: {
        component: LinkBehavior,
      } as LinkProps,
    },
    MuiListItemButton: {
      defaultProps: {
        LinkComponent: LinkBehavior,
      },
    },
    MuiButtonBase: {
      defaultProps: {
        LinkComponent: LinkBehavior,
      },
    },
  },
})

const fontHeader = {
  color: rawTheme.palette.text.primary,
  fontWeight: 400,
  textAlign: 'left',
  fontFamily: '"Audiowide", cursive !important',
}

export const theme = {
  ...rawTheme,
  typography: {
    ...rawTheme.typography,
    fontHeader,
    h1: {
      ...rawTheme.typography.h1,
      ...fontHeader,
      letterSpacing: 0,
      fontSize: 20,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '2.5rem!important',
      },
    },
    h2: {
      ...rawTheme.typography.h2,
      ...fontHeader,
      fontSize: 20,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '2.1rem!important',
      },
    },
    h3: {
      ...rawTheme.typography.h3,
      ...fontHeader,
      fontSize: 18,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '2rem!important',
      },
    },
    h4: {
      ...rawTheme.typography.h4,
      ...fontHeader,
      fontSize: 18,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '1.9rem!important',
      },
    },
    h5: {
      ...rawTheme.typography.h5,
      ...fontHeader,
      fontSize: 16,
      fontWeight: rawTheme.typography.fontWeightLight,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '1.5rem!important',
      },
    },
    h6: {
      ...rawTheme.typography.h6,
      ...fontHeader,
      fontSize: 16,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '1.1rem!important',
      },
    },
    subtitle1: {
      ...rawTheme.typography.subtitle1,
      ...fontHeader,
      fontSize: 16,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '1.1rem!important',
      },
    },
    body1: {
      ...rawTheme.typography.body2,
      ...fontHeader,
      fontWeight: rawTheme.typography.fontWeightRegular,
      fontSize: 16,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '1rem!important',
      },
    },
    body2: {
      ...fontHeader,
      ...rawTheme.typography.body1,
      fontSize: 14,
      [rawTheme.breakpoints.down('sm')]: {
        fontSize: '.9rem!important',
      },
    },
  },
}
