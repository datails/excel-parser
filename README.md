# Excel Formatter
A formatter tool to split columns in Excel. Project powered by [Wails](https://wails.io/). Front-end hosted via [firebase](https://excel-formatter.firebaseapp.com).

## Development
Make sure both the `go` and `node` runtime are installed.

```bash
wails dev
```

## Build

```bash
wails build -platform windows/amd64 -o excel-formatter-amd64.exe

wails build -platform windows/arm64 -o excel-formatter-arm64.exe

wails build -platform darwin/universal
```