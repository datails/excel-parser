package main

import (
	"encoding/json"
	"strings"
)

type Conditions struct {
	Id        string
	Value     string
	Condition string
	IsFirst   bool
	Children  []Conditions
	Text      string
}

type ComplexFormat struct {
	CreatedAt int
	Worksheet string
	Workbook  string
	Target    string
	Rows      [][]string
	Delimiter []Conditions
}

type jsondata []string

func (j *jsondata) delete(selector string) {
	var r jsondata
	for _, str := range *j {
		if str != selector {
			r = append(r, str)
		}
	}
	*j = r
}

func rowAppend(newRow *[]string, c string, d []string) {
	nC := strings.Split(c, strings.Join(d, ""))
	*newRow = append(*newRow, nC...)
}

func (a *App) ComplexFormatRow(row *[]string, parent []string, cond Conditions) []string {
	newRow := []string{}
	delimiter := append(parent, cond.Value)

	if len(cond.Children) == 0 {
		if cond.IsFirst == true || cond.Condition == "AND" {
			for _, c := range *row {
				rowAppend(&newRow, c, delimiter)
			}
		} else if cond.Condition == "OR" {
			for _, c := range *row {
				if strings.Contains(c, strings.Join(parent[:], "")) {
					rowAppend(&newRow, c, delimiter)
				} else {
					newColumn := strings.Split(c, strings.Join(append(parent[:len(parent)-1], cond.Value), ""))
					newRow = append(newRow, newColumn...)
				}
			}
		}
	} else {
		for _, cond := range cond.Children {
			newRow = append(newRow, a.ComplexFormatRow(row, delimiter, cond)...)
		}
	}

	// remove empty columns
	j := jsondata(newRow)
	j.delete("")

	return j
}

func (a *App) ComplexFormatSheet(v string) (*ComplexFormat, error) {
	d := ComplexFormat{}
	newRows := [][]string{}

	if err := json.Unmarshal([]byte(v), &d); err != nil {
		return nil, err
	}

	rows, err := a.ReadFile(d.Workbook, d.Worksheet)

	if err != nil {
		return nil, err
	}

	for _, row := range rows {
		for _, v := range d.Delimiter {
			newRows = append(newRows, a.ComplexFormatRow(&row, []string{v.Text}, v))
		}
	}

	d.Rows = newRows

	return &d, nil
}

func (a *App) FormatComplexExcel(value string) error {
	formatted, err := a.ComplexFormatSheet(value)

	if err != nil {
		return err
	}

	a.Save(formatted.Target, formatted.Workbook, formatted.Worksheet, &formatted.Rows)

	return nil
}
