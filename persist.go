package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/google/uuid"

	badger "github.com/dgraph-io/badger/v3"
)

const (
	STORAGE_PATH = "/tmp/badger"
)

type Rule struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func (a *App) SetItem(prefix string, value string) error {
	err := a.Set(prefix+uuid.New().String(), value)

	if err != nil {
		return err
	}

	return nil
}

func (a *App) Set(id string, value string) error {
	opts := badger.DefaultOptions(STORAGE_PATH).WithSyncWrites(true)

	db, err := badger.Open(opts)

	if err != nil {
		return err
	}

	defer db.Close()

	err = db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(id), []byte(value))
		return err
	})

	if err != nil {
		return err
	}

	return nil
}

func GetFromView(key string, valCopy *[]byte) func(txn *badger.Txn) error {
	return func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(key))

		if err != nil {
			return err
		}

		err = item.Value(func(val []byte) error {
			*valCopy = append([]byte{}, val...)
			return nil
		})

		if err != nil {
			return err
		}

		return nil
	}
}

func DelFromView(key string) func(txn *badger.Txn) error {
	return func(txn *badger.Txn) error {
		err := txn.Delete([]byte(key))

		if err != nil {
			return err
		}

		return nil
	}
}

func (a *App) Reset() error {
	db, err := badger.Open(badger.DefaultOptions(STORAGE_PATH))

	if err != nil {
		return err
	}

	defer db.Close()

	if err := db.DropAll(); err != nil {
		return err
	}

	return nil
}

func (a *App) ResetPrefix(prefix string) error {
	db, err := badger.Open(badger.DefaultOptions(STORAGE_PATH))

	if err != nil {
		return err
	}

	defer db.Close()

	if err := db.DropPrefix([]byte(prefix)); err != nil {
		return err
	}

	return nil
}

func (a *App) Delete(key string) error {
	db, err := badger.Open(badger.DefaultOptions(STORAGE_PATH))

	if err != nil {
		return err
	}

	defer db.Close()

	if err := db.View(DelFromView(key)); err != nil {
		return err
	}

	return nil
}

func (a *App) Get(key string) (string, error) {
	db, err := badger.Open(badger.DefaultOptions(STORAGE_PATH))

	if err != nil {
		return "", nil
	}

	defer db.Close()

	var valCopy []byte

	if err := db.View(GetFromView(key, &valCopy)); err != nil {
		return "", err
	}

	return string(valCopy), nil
}

func handleValue(key string, keyPair *[]string) func(v []byte) error {
	return func(v []byte) error {
		curr := &Rule{Key: key, Value: string(v)}

		b, err := json.Marshal(curr)

		if err != nil {
			fmt.Print("Something went wrong", errors.New((err.Error())))
		}

		*keyPair = append(*keyPair, string(b))
		return nil
	}
}

func Loop(keyPair *[]string, pref []byte) func(txn *badger.Txn) error {
	return func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		for it.Seek(pref); it.ValidForPrefix(pref); it.Next() {
			item := it.Item()
			k := item.Key()

			if err := item.Value(handleValue(string(k), keyPair)); err != nil {
				return err
			}
		}

		return nil
	}
}

func (a *App) List(prefix string) ([]string, error) {
	db, err := badger.Open(badger.DefaultOptions(STORAGE_PATH))
	keyPair := &[]string{}

	if err != nil {
		return *keyPair, err
	}

	defer db.Close()

	pref := []byte(prefix)

	err = db.View(Loop(keyPair, pref))

	if err != nil {
		return nil, err
	}

	return *keyPair, nil
}
